import json
import os.path
from datetime import datetime as dt

from aiohttp import web

news = "news.json"
comments = "comments.json"


def read_data():
    if os.path.isfile(news):
        with open('news.json') as f:
            news_data = json.load(f)

    if os.path.isfile(comments):
        with open('comments.json') as f:
            comments_data = json.load(f)

    return news_data, comments_data


news_data, comments_data = read_data()


async def handle(request):
    _id = request.match_info.get('id', None)

    data = {"news": []}
    for news_item in news_data["news"]:
        if not news_item.get("deleted", False) and dt.strptime(news_item["date"], '%Y-%m-%dT%H:%M:%S') < dt.now():
            comments_count = sum(value["news_id"] == news_item["id"] for value in comments_data["comments"])
            news_item["comments_count"] = comments_count
            news_item["date"] = dt.strptime(news_item["date"], '%Y-%m-%dT%H:%M:%S').isoformat()
            data["news"].append(news_item)

    data["news"] = sorted(data["news"], key=lambda k: k['date'])

    if _id:
        try:
            _id = int(_id)
        except ValueError:
            _id = None

        if not _id:
            return web.HTTPNotFound()

        news_item = next(filter(lambda item: item["id"] == int(_id), data["news"]), None)

        if not news_item:
            return web.HTTPNotFound()

        news_item["comments"] = []
        current_comments = []

        for comment in comments_data["comments"]:
            if not comment.get("deleted", False) and dt.strptime(comment["date"], '%Y-%m-%dT%H:%M:%S') < dt.now():
                if comment["news_id"] == int(_id):
                    current_comments.append(comment)

        news_item["comments"] = current_comments
        return web.json_response(news_item)

    return web.json_response(data) if len(data) > 0 else web.HTTPNotFound


app = web.Application()
app.add_routes([web.get('/', handle),
                web.get('/news/{id}', handle)])

web.run_app(app)
